# LIS4368 Advanced Web Applications

## Steve Walter

### Project 2 Requirements:

*Four Parts:*

1. Clone assignment files
2. Modify files to allow server-side validation
3. Modify files to allow database to add, edit, and delete data entry
4. Questions

#### README.md file should include the following items:

* Screenshot of Valid User Form Entry
* Screenshot of Passed Validation
* Screenshot of Display Data
* Screenshot of Modify Form
* Screenshot of Modified Data
* Screenshot of Delete Warning
* Screenshot of Associated Database Changes (Select, Insert, Update, Delete)

#### Assignment Screenshots:

| *Screenshot of Valid User Form Entry:* | *Screenshot of Passed Validation:* |
|---------------------------:|:-----------------------------------|
| ![Valid User Form Entry](img/1.png) | ![Passed Validation](img/2.png) |

| *Screenshot of Display Data:* | *Screenshot of Modify Form* |
|---------------------------:|:-----------------------------------|
| ![Display Data](img/3.png) | ![Modify Form](img/4.png) |

| *Screenshot of Modified Data:* | *Screenshot of Delete Warning* |
|---------------------------:|:-----------------------------------|
| ![Modified Data](img/5.png) | ![Delete Warning](img/6.png) |

*Screenshot of Associated Database Changes (Select, Insert, Update, Delete)*
![Associated Database Changes](img/7.png)