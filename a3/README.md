# LIS4368 Advanced Web Applications

## Steve Walter

### Assignment 3 Requirements:

*three Parts:*

1. Create a database and an EDR
2. Fill database with data
3. Questions

#### README.md file should include the following items:

* Screenshot of ERD
* Link to a3.mwb
* Link to a3.sql

#### Assignment Screenshots:

*Screenshot of Assignment 3 ERD*:

![Assignment 3 ERD](img/a3.png)

*Links for Assignment 3*:

[Link for a3.mwb](links/a3.mwb "Link for a3.mwb")

[Link for a3.sql](links/a3.sql "Link for a3.sql")