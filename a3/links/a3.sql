-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema slw16c
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `slw16c` ;

-- -----------------------------------------------------
-- Schema slw16c
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `slw16c` DEFAULT CHARACTER SET utf8 ;
SHOW WARNINGS;
USE `slw16c` ;

-- -----------------------------------------------------
-- Table `slw16c`.`petstore`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `slw16c`.`petstore` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `slw16c`.`petstore` (
  `pst_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `pst_name` VARCHAR(30) NOT NULL,
  `pst_street` VARCHAR(30) NOT NULL,
  `pst_city` VARCHAR(30) NOT NULL,
  `pst_state` CHAR(2) NOT NULL,
  `pst_zip` INT(9) UNSIGNED NOT NULL,
  `pst_phone` BIGINT UNSIGNED NOT NULL,
  `pst_email` VARCHAR(100) NOT NULL,
  `pst_url` VARCHAR(100) NOT NULL,
  `pst_ytd_sales` DECIMAL(10,2) UNSIGNED NOT NULL,
  `pst_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`pst_id`))
ENGINE = InnoDB
COMMENT = '				';

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `slw16c`.`customer`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `slw16c`.`customer` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `slw16c`.`customer` (
  `cus_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `cus_fname` VARCHAR(30) NOT NULL,
  `cus_lname` VARCHAR(45) NOT NULL,
  `cus_street` VARCHAR(30) NOT NULL,
  `cus_city` VARCHAR(30) NOT NULL,
  `cus_state` CHAR(2) NOT NULL,
  `cus_zip` INT(9) UNSIGNED NOT NULL,
  `cus_phone` BIGINT UNSIGNED NOT NULL,
  `cus_email` VARCHAR(100) NOT NULL,
  `cus_balance` VARCHAR(100) NOT NULL,
  `cus_total_sales` DECIMAL(10,2) UNSIGNED NOT NULL,
  `cus_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`cus_id`))
ENGINE = InnoDB
COMMENT = '				';

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `slw16c`.`pet`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `slw16c`.`pet` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `slw16c`.`pet` (
  `pet_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `pst_id` SMALLINT UNSIGNED NOT NULL,
  `cus_id` SMALLINT UNSIGNED NULL,
  `pet_type` VARCHAR(45) NOT NULL,
  `pet_sex` ENUM('m', 'f') NOT NULL,
  `pet_cost` DECIMAL(6,2) UNSIGNED NOT NULL,
  `pet_price` DECIMAL(6,2) UNSIGNED NOT NULL,
  `pet_age` TINYINT UNSIGNED NOT NULL,
  `pet_color` VARCHAR(30) NOT NULL,
  `pet_sale_date` DATE NOT NULL,
  `pet_vaccine` ENUM('y', 'n') NOT NULL,
  `pet_neuter` ENUM('y', 'n') NOT NULL,
  `pet_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`pet_id`),
  INDEX `fk_pet_petstore_idx` (`pst_id` ASC),
  INDEX `fk_pet_customer1_idx` (`cus_id` ASC),
  CONSTRAINT `fk_pet_petstore`
    FOREIGN KEY (`pst_id`)
    REFERENCES `slw16c`.`petstore` (`pst_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_pet_customer1`
    FOREIGN KEY (`cus_id`)
    REFERENCES `slw16c`.`customer` (`cus_id`)
    ON DELETE SET NULL
    ON UPDATE CASCADE)
ENGINE = InnoDB;

SHOW WARNINGS;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `slw16c`.`petstore`
-- -----------------------------------------------------
START TRANSACTION;
USE `slw16c`;
INSERT INTO `slw16c`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (1, 'Pets R Us', '123 South Street', 'Tallahassee', 'fl', 32304, 8501234567, 'pets@mail.com', 'petsrus.com', 34000, NULL);
INSERT INTO `slw16c`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (2, 'U Buy Pets', '456 North Road', 'Miami', 'fl', 33101, 3052343265, 'ubuy@mail.com', 'ubuypets.com', 42000, NULL);
INSERT INTO `slw16c`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (3, 'We Have Pets', '213 That Way', 'Atlanta', 'ga', 30303, 4049834265, 'wehave@mail.com', 'wehavepets.com', 23000, NULL);
INSERT INTO `slw16c`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (4, 'Take R Critters', '954 Yello Brick Rd', 'New York', 'ny', 10001, 7188351232, 'taker@mail.com', 'takercritters.com', 102000, NULL);
INSERT INTO `slw16c`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (5, 'Dogz N Catz', '932 North Way', 'Saint Petersburg', 'fl', 33713, 7272123418, 'dogzncatz@mail.com', 'dogzncatz.com', 36000, NULL);
INSERT INTO `slw16c`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (6, 'The Wet Dog', '2011 Johnson St', 'Jacksonville', 'fl', 32034, 9046659982, 'wetdog@mail.com', 'thewetdog.com', 34500, NULL);
INSERT INTO `slw16c`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (7, 'Wag A Tail', '351 South Central', 'Tallahassee', 'fl', 32304, 8505565554, 'wag@mail.com', 'wagatail.com', 26500, NULL);
INSERT INTO `slw16c`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (8, 'Pet Shoppe', '512 Main Street', 'Miami', 'fl', 33101, 3054434487, 'theshoppe@mail.com', 'petshoppe.com', 49500, NULL);
INSERT INTO `slw16c`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (9, 'Bark Fest', '8443 Conn Rd', 'Atlanta', 'ga', 30303, 4049982231, 'bark@mail.com', 'barkfest.com', 55500, NULL);
INSERT INTO `slw16c`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (10, 'Ankle Biters', '1212 Flower St', 'Jacksonville', 'fl', 32034, 9045459133, 'ankelbit@mail.com', 'anklebitters.com', 100000, NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `slw16c`.`customer`
-- -----------------------------------------------------
START TRANSACTION;
USE `slw16c`;
INSERT INTO `slw16c`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (1, 'John', 'Smith', '123 Green Rd', 'Tallahassee', 'fl', 32304, 8509876543, 'smithj@mail.com', '0', 1500, NULL);
INSERT INTO `slw16c`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (2, 'Jack', 'Johns', '234 Blue St', 'Miami', 'fl', 33101, 3059764852, 'johnsj@mail.com', '0', 1200, NULL);
INSERT INTO `slw16c`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (3, 'Jan', 'Gould', '345 Jumpper Ln', 'Atlanta', 'ga', 30303, 4042224448, 'gouldj@mail.com', '0', 500, NULL);
INSERT INTO `slw16c`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (4, 'Kyle', 'Slater', '456 Concrete Way', 'New York', 'ny', 10001, 7185622224, 'slaterk@mail.com', '0', 1000, NULL);
INSERT INTO `slw16c`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (5, 'Brian', 'Evans', '567 Orange Way', 'Saint Petersburg', 'fl', 33713, 7277777778, 'evansb@mail.com', '0', 250, NULL);
INSERT INTO `slw16c`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (6, 'Bob', 'Builder', '678 Brown St', 'Jacksonville', 'fl', 32034, 9046666589, 'builderb@mail.com', '0', 950, NULL);
INSERT INTO `slw16c`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (7, 'Lianne', 'Rice', '789 Whipp Ln', 'Tallahassee', 'fl', 32304, 8502003000, 'ricel@mail.com', '0', 80, NULL);
INSERT INTO `slw16c`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (8, 'Kelly', 'Doe', '890 Street St', 'Miami', 'fl', 33101, 3054008000, 'doek@mail.com', '0', 120, NULL);
INSERT INTO `slw16c`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (9, 'Kim', 'Kar', '901 Cup Rd', 'Atlanta', 'ga', 30303, 4044044044, 'kark@mail.com', '0', 412, NULL);
INSERT INTO `slw16c`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (10, 'George', 'Jungle', '100 Tree Way', 'Jacksonville', 'fl', 32034, 9049874000, 'jungleg@mail.com', '0', 60, NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `slw16c`.`pet`
-- -----------------------------------------------------
START TRANSACTION;
USE `slw16c`;
INSERT INTO `slw16c`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (1, 1, 1, 'Dog', 'm', 1500, 1500, 1, 'black', '2017-03-12', 'y', 'y', NULL);
INSERT INTO `slw16c`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (2, 2, 2, 'Dog', 'm', 1200, 1200, 2, 'white', '2017-02-12', 'y', 'y', NULL);
INSERT INTO `slw16c`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (3, 3, 3, 'Cat', 'f', 500, 500, 1, 'orange', '2017-01-12', 'y', 'y', NULL);
INSERT INTO `slw16c`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (4, 4, 4, 'Dog', 'f', 1000, 1000, 1, 'brown', '2017-12-12', 'y', 'y', NULL);
INSERT INTO `slw16c`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (5, 5, 5, 'Bird', 'f', 250, 250, 2, 'green', '2017-11-12', 'n', 'n', NULL);
INSERT INTO `slw16c`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (6, 6, 6, 'Dog', 'f', 950, 950, 2, 'grey', '2017-10-12', 'y', 'y', NULL);
INSERT INTO `slw16c`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (7, 7, 7, 'Cat', 'm', 80, 80, 3, 'white', '2017-09-12', 'y', 'y', NULL);
INSERT INTO `slw16c`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (8, 8, 8, 'Cat', 'f', 120, 120, 6, 'white', '2017-08-12', 'y', 'y', NULL);
INSERT INTO `slw16c`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (9, 9, 9, 'Cat', 'm', 412, 412, 3, 'black', '2017-07-12', 'y', 'y', NULL);
INSERT INTO `slw16c`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (10, 10, 10, 'Lizzard', 'f', 60, 60, 1, 'green', '2017-06-12', 'n', 'n', NULL);

COMMIT;

