# LIS4368 Advanced Web Applications

## Steve Walter

### LIS4368 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")

    * Install JDK
    * Install Tomcat
    * Provide screenshots of installations
    * Create Bitbucket repo
    * Complete Bitbucket tutorials
    * Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")

    * Connect to MySql
    * Create files for servlet
    * Connect MySql Database to Tomcat

3. [A3 README.md](a3/README.md "My A3 README.md file")

    * Creating a local connection for mySQL
    * Sketching out database per business rules
    * Creating ERD for database and filling it with data
    * Foward-Engineering the ERD

4. [A4 README.md](a4/README.md "My A4 README.md file")

    * Cloned student files
    * Edited file to check for server-side data validation
    * Used regexp to only allow appropriate characters for each control
    * Compile both java files

5. [A5 README.md](a5/README.md "My A5 README.md file")

    * Include server-side validation from A4
    * Add code to servlet files for SQL
    * Compiling servlet files
    * Allow SQL Database to ADD Data Entry

6. [P1 README.md](p1/README.md "My P1 README.md file")

    * Cloned student files
    * Edited file to check data validation
    * Used regexp to only allow appropriate characters for each control

7. [P2 README.md](p2/README.md "My P2 README.md file")

    * Cloned student files
    * Edited file to check data validation
    * Used regexp to only allow appropriate characters for each control
    * Modify files to allow database to add, edit, and delete data entry