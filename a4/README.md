# LIS4368 Advanced Web Applications

## Steve Walter

### Assignment 4 Requirements:

*Three Parts:*

1. Clone assignment files
2. Modify files to allow server-side validation
3. Questions

#### README.md file should include the following items:

* Screenshot of Failed Validation
* Screenshot of Passed Validation

#### Assignment Screenshots:

| *Screenshot of Failed Validation:* | *Screenshot of Passed Validation:* |
|---------------------------:|:-----------------------------------|
| ![Failed Validation](img/1.png) | ![Passed Validation](img/2.png) |