<!DOCTYPE html>
<html lang="en">
<head>
<!--
"Time-stamp: <Sun, 02-19-17, 12:36:56 Eastern Standard Time>"
//-->
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="My online portfolio">
	<meta name="author" content="Steve Walter">
	<link rel="icon" href="icon.png">

	<title>LIS4368 - Assignment1</title>

	<%@ include file="/css/include_css.jsp" %>		
	<style type="text/css">

		body
		{
		background-color: #ccccb3
		}
		.text-justify 
		{
			font-size: 20px
		}
		h4
		{
			font-size: 20px
		}
		*{
	 box-sizing: border-box;
   }
   
   .column {
	 float: left;
	 width: 33.33%;
	 padding: 5px;
   }
   
   /* Clearfix (clear floats) */
   .row::after {
	 content: "";
	 clear: both;
	 display: table;
   }
	</style>
</head>
<body>

<!-- display application path -->
<% //= request.getContextPath()%>
	
<!-- can also find path like this...<a href="${pageContext.request.contextPath}${'/a5/index.jsp'}">A5</a> -->

	<%@ include file="/global/nav.jsp" %>	

	<div class="container">
		<div class="starter-template">
			<div class="row">
				<div class="col-sm-8 col-sm-offset-2">
					
					<div class="page-header">
						<%@ include file="global/header.jsp" %>
					</div>

					<h4><strong>Directories:</strong></h4>
					<img src="img/1hello.png" class="img-responsive center-block" alt="hello">
					&nbsp;
					<h4><strong>Hello:</strong></h4>
					<img src="img/2index.png" class="img-responsive center-block" alt="index">
					&nbsp;
					<h4><strong>Hello, World!:</strong></h4>
					<img src="img/3helloworld.png" class="img-responsive center-block" alt="helloworld">
					&nbsp;
					<h4><strong>Hello World, Again!:</strong></h4>
					<img src="img/5helloworldagain.png" class="img-responsive center-block" alt="helloworldagain">
					&nbsp;
					<h4><strong>Database unselected:</strong></h4>
					<img src="img/4unselected.png" class="img-responsive center-block" alt="unselected">
					&nbsp;
					<h4><strong>Database Selected:</strong></h4>
					<img src="img/6selected.png" class="img-responsive center-block" alt="selected">
					&nbsp;
					<h4><strong>Database Results:</strong></h4>
					<img src="img/results.png" class="img-responsive center-block" alt="results">

				</div>
			</div>

	<%@ include file="/global/footer.jsp" %>

	</div> <!-- end starter-template -->
 </div> <!-- end container -->

 	<%@ include file="/js/include_js.jsp" %>		

</body>
</html>
