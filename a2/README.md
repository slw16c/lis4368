# LIS4368 Advanced Web Applications

## Steve Walter

### Assignment 2 Requirements:

*two Parts:*

1. Development Installations
2. Questions

#### README.md file should include the following items:

* Assessment Links
* Screenshot of query results

#### Assessment Links:

1. [localhost:9999/hello](http://localhost:9999/hello "localhost:9999/hello")
2. [localhost:9999/HelloHome](http://localhost:9999/hello/HelloHome.html "localhost:9999/hello/HelloHome")
3. [localhost:9999/hello/sayhello](http://localhost:9999/hello/sayhello "localhost:9999/hello/sayhello")
4. [localhost:9999/hello/querybook](http://localhost:9999/hello/querybook.html "localhost:9999/hello/querybooke")
5. [localhost:9999/hello/sayhi](http://localhost:9999/hello/sayhi "localhost:9999/hello/sayhi")

#### Assignment Screenshots:

| *Screenshot of running localhost:9999/Hello*  | *Screenshot of running index.html*            |
|:---------------------------------------------:|:---------------------------------------------:|
|![JDK Installation Screenshot](img/1hello.png) |![JDK Installation Screenshot](img/2index.png) |

| *Screenshot of running Hello World*           | *Screenshot of running Hello World Again*     |
|:---------------------------------------------:|:---------------------------------------------:|
|![JDK Installation Screenshot](img/3helloworld.png)|![JDK Installation Screenshot](img/5helloworldagain.png)|

| *Screenshot of running query*                 | *Screenshot of the query running*             |
|:---------------------------------------------:|:---------------------------------------------:|
|![JDK Installation Screenshot](img/6selected.png)|![JDK Installation Screenshot](img/results.png)|