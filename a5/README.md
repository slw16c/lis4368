# LIS4368 Advanced Web Applications

## Steve Walter

### Assignment 5 Requirements:

*Four Parts:*

1. Clone assignment files
2. Modify files to allow server-side validation
3. Modify files to allow database to add data entry
4. Questions

#### README.md file should include the following items:

* Screenshot of Valid User Form Entry
* Screenshot of Passed Validation
* Screenshot of Associated Database Entry

#### Assignment Screenshots:

| *Screenshot of Valid User Form Entry:* | *Screenshot of Passed Validation:* |
|---------------------------:|:-----------------------------------|
| ![Valid User Form Entry](img/1.png) | ![Passed Validation](img/2.png) |

*Screenshot of Associated User Form Entry:*

![Associated User Form Entry](img/3.png)