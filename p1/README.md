# LIS4368 Advanced Web Applications

## Steve Walter

### Project 1 Requirements:

*Four Parts:*

1. Clone assignment files
2. Modify files according to directions
3. Add code to jQuery
4. Questions

#### README.md file should include the following items:

* Screenshot of main page
* Screenshot of Failed Validation
* Screenshot of Passed Validation

#### Assignment Screenshots:

| *Screenshot of Main Page*: | *Screenshot of Failed Validation*: |
|---------------------------:|:-----------------------------------|
| ![LIS 4368 Main Page](img/ss1.png) | ![Failed Validation](img/ss2.png) |

*Screenshot of Passed Validation*:

![Passed Validation](img/ss3.png)