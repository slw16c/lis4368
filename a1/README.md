# LIS4368 Advanced Web Applications

## Steve Walter

### Assignment 1 Requirements:

*three Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Java/JSP/Servlet Development Installation
3. Questions

| README.md file should include the following items | Git commands w/short descriptions:                                                       |
|:---                                               |:---                                                                                      |
| * Screenshot of running JDK java Hello            | 1. git init - Creates a new local repository                                             |
| * Screenshot of running http://localhost9999      | 2. git status - Lists the files you've changed and those you still need to add or commit |
| * Git commands with short descriptions            | 3. git add - Adds one or more files to staging                                           |
| * Bitbucket repo links                            | 4. git commit - Commits the changes to the head                                          |
|                                                   | 5. git push - Sends the changes to the master branch of remote repository                |
|                                                   | 6. git pull - Fetch and merge changes on the remote server to your working directory     |
|                                                   | 7. git clone - Creates a working of a local repository                                   |

#### Assignment Screenshots:

| *Screenshot of running java Hello*             | *Screenshot of http://localhost9999*         |
|:----------------------------------------------:|:---------------------------------------------|
| ![JDK Installation Screenshot](img/javass.png) | ![Tomcat running Screenshot](img/tomcat.png) |